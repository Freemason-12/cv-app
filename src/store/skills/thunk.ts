import { apiAddress } from "../../constants";
import {
	addSkillAction,
	setSkillsListAction,
	throwSkillErrorAction,
} from "./actions";
import { Skill } from "./types";

export const setSkillsList = () => async (dispatch: any) => {
	const response = await fetch(`${apiAddress}/api/skills`).then((r) =>
		r.json(),
	);
	if (response.error) dispatch(throwSkillErrorAction());
	else dispatch(setSkillsListAction(response.skills));
};

export const addSkill = (skill: Skill) => async (dispatch: any) => {
	const response = await fetch(`${apiAddress}/api/skills`, {
		method: "POST",
		headers: { "Content-Type": "application/json" },
		body: JSON.stringify(skill),
	}).then((r) => r.json);
	console.log(response);
	// if(!response.successful)
	dispatch(addSkillAction(skill));
};
