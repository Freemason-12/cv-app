import { createAction } from "@reduxjs/toolkit";
import { Skill } from "./types";
export const addSkillAction = createAction<Skill | undefined>("ADD_SKILL");

export const getSkillsAction = createAction("GET_SKILLS");

export const setSkillsListAction = createAction<Skill[] | undefined>(
	"SET_SKILL_LIST",
);

export const throwSkillErrorAction = createAction<Skill[] | undefined>(
	"THROW_SKILL_ERROR",
);

export const setPendingAction = createAction<Skill[] | undefined>(
	"SET_PENDING",
);
