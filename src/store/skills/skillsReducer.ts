import { initialState } from "../store";
export function skillsReducer(skills = initialState.skills, action: any) {
	switch (action.type) {
		case "GET_SKILLS":
			return { state: "fullfilled", all: skills.all };
		case "ADD_SKILL":
			return {
				state: "fullfilled",
				all: [...skills.all, action.payload],
			};
		case "SET_SKILL_LIST":
			return { state: "fullfilled", all: action.payload };
		case "THROW_SKILL_ERROR":
			return { state: "error", all: [] };
		case "SET_PENDING":
			return { state: "pending", all: skills.all };
		default:
			return skills;
	}
}
