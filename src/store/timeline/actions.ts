import { createAction } from "@reduxjs/toolkit";
import { Timestamp } from "./types";

export const setTimeLineAction = createAction<Timestamp[] | undefined>(
	"SET_TIMELINE",
);

export const throwTimeLineErrorAction = createAction<Timestamp[] | undefined>(
	"THROW_TIMELINE_ERROR",
);
