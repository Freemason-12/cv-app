import { initialState } from "../store";
export function timelineReducer(timeline = initialState.timeline, action: any) {
	switch (action.type) {
		case "SET_TIMELINE":
			return { state: "fullfilled", all: action.payload };
		case "THROW_TIMELINE_ERROR": {
			console.log("error thrown");
			return { state: "error", all: [] };
		}
		default:
			return timeline;
	}
}
