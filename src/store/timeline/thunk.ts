import { setTimeLineAction, throwTimeLineErrorAction } from "./actions";
import { apiAddress } from "../../constants";
export const setTimeline = () => async (dispatch: any) => {
	const response = await fetch(`${apiAddress}/api/timestamp`).then((r) =>
		r.json(),
	);
	if (response.error) dispatch(throwTimeLineErrorAction());
	else dispatch(setTimeLineAction(response.timestamps));
};
