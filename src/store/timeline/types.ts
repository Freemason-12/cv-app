export interface Timestamp {
	title: string;
	text: string;
	date: number;
}
