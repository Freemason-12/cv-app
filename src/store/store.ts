import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { useDispatch } from "react-redux";
import { skillsReducer } from "./skills/skillsReducer";
import { Skill } from "./skills/types";
import { timelineReducer } from "./timeline/timelineReducer";
import { Timestamp } from "./timeline/types";

export type FieldState = "pending" | "fullfilled" | "error";

export interface AppState {
	skills: { state: FieldState; all: Skill[] };
	timeline: { state: FieldState; all: Timestamp[] };
}

export const initialState: AppState = {
	skills: { state: "pending", all: [] as Skill[] },
	timeline: { state: "pending", all: [] as Timestamp[] },
};

const rootReducer = combineReducers({
	skills: skillsReducer,
	timeline: timelineReducer,
});

export const store = configureStore({
	preloadedState: initialState,
	reducer: rootReducer,
});

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = useDispatch.withTypes<AppDispatch>();
