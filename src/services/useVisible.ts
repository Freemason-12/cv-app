import { MutableRefObject, useMemo, useState, useEffect } from "react";

export function useVisible(ref: MutableRefObject<HTMLElement | null>) {
  const [isVisible, setIsVisible] = useState(false);
  const observer = useMemo(
    () =>
      new IntersectionObserver(
        ([entry]) => setIsVisible(entry.isIntersecting),
        { rootMargin: "0% 0% -90% 0%" },
      ),
    [],
  );
  useEffect(() => {
    if (ref.current) observer.observe(ref.current as Element);
    return () => observer.disconnect();
  }, [observer, ref]);
  return {
    isVisible,
    setFocus: () => ref.current?.focus(),
  };
}
