import { createServer, Model, Response } from "miragejs";
import { apiAddress } from "../constants";

export function startServer({ environment = "test" } = {}) {
  return createServer({
    environment,
    models: {
      skill: Model,
      timestamp: Model,
    },
    routes() {
      this.timing = 3000;
      this.urlPrefix = apiAddress;
      this.namespace = "/api";
      this.get("/skills", (schema) => {
        // Uncomment line below to emulate Skills component showing error
        // return new Response(400, {}, { error: "Error fetching data" });

        // Uncomment line below to emulate Skills component showing loading succesfully
        return schema.all("skill");
      });
      this.post("/skills", (schema, request) => {
        schema.create("skill", JSON.parse(request.requestBody));
        localStorage.setItem("skills", JSON.stringify(schema.all("skill")));
        return { successful: true };
      });

      this.get("/timestamp", (schema) => {
        // Uncomment line below to emulate timeline showing error
        return new Response(400, {}, { error: "Error fetching data" });

        // Uncomment line below to emulate Skills component showing loading succesfully
        // return schema.all("timestamp");
      });
    },
    seeds(server) {
      server.logging = true;
      if (localStorage.getItem("skills")) {
        const skills = JSON.parse(
          String(localStorage.getItem("skills")),
        ).models;
        skills.map((skill: any) => server.create("skill", skill));
      }

      server.create("timestamp", {
        date: 2001,
        title: "Title 0",
        text: "Elit voluptate ad nostrud laboris. Elit incididunt mollit enim enim id id laboris dolore et et mollit. Mollit adipisicing ullamco exercitation ullamco proident aute enim nisi. Dolore eu fugiat consectetur nulla sunt Lorem ex ad. Anim eiusmod do tempor fugiat minim do aliqua amet ex dolore velit.\r\n",
      } as object);
      server.create("timestamp", {
        date: 2000,
        title: "Title 1",
        text: "Et irure culpa ad proident labore excepteur elit dolore. Quis commodo elit culpa eiusmod dolor proident non commodo excepteur aute duis duis eu fugiat. Eu duis occaecat nulla eiusmod non esse cillum est aute elit amet cillum commodo.\r\n",
      } as object);
      server.create("timestamp", {
        date: 2012,
        title: "Title 2",
        text: "Labore esse tempor nisi non mollit enim elit ullamco veniam elit duis nostrud. Enim pariatur ullamco dolor eu sunt ad velit aute eiusmod aliquip voluptate. Velit magna labore eiusmod eiusmod labore amet eiusmod. In duis eiusmod commodo duis. Exercitation Lorem sint do aliquip veniam duis elit quis culpa irure quis nulla. Reprehenderit fugiat amet sint commodo ex.\r\n",
      } as object);
    },
  });
}
