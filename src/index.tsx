import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./index.css";
import { Home } from "./pages/Home/Home";
import { Inner } from "./pages/Inner/Inner";
// import App from './App';
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { store } from "./store/store";
import { startServer } from "./services/server";

if (process.env.NODE_ENV === "development")
	startServer({ environment: "development" });

console.log(process.env.NODE_ENV);
const root = ReactDOM.createRoot(
	document.getElementById("root") as HTMLElement,
);
root.render(
	<Provider store={store}>
		<BrowserRouter>
			<React.StrictMode>
				<Routes>
					<Route path="/" element={<Home />} />
					<Route path="/inner" element={<Inner />} />
				</Routes>
			</React.StrictMode>
		</BrowserRouter>
	</Provider>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
