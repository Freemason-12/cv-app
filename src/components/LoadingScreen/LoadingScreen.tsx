import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowsRotate } from "@fortawesome/free-solid-svg-icons";
import styles from "./LoadingScreen.module.scss";

export const LoadingScreen: React.FC = () => (
  <div className={styles.loading}>
    <FontAwesomeIcon icon={faArrowsRotate} title="loading-screen" />
  </div>
);
