import { render, screen } from "@testing-library/react";
import { LoadingScreen } from "./LoadingScreen";

describe("LoadingScreen component", () => {
  it("should show a proper loading screen", () => {
    render(<LoadingScreen />);
    expect(screen.getByTitle("loading-screen")).toBeInTheDocument();
  });
});
