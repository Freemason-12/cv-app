import { fireEvent, render, screen } from "@testing-library/react";
import { FlagButton } from "./FlagButton";
import { faPencil } from "@fortawesome/free-solid-svg-icons";

describe("FlagButton component", () => {
  it("sholud be displayed and should trigger click function", () => {
    const clicked = jest.fn();
    render(<FlagButton icon={faPencil} onClick={clicked} direction="left" />);
    expect(screen.getByRole("button")).toBeInTheDocument();

    fireEvent.click(screen.getByRole("button"));
    expect(clicked).toBeCalled();
  });
});
