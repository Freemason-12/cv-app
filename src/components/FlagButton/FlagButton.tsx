import React from "react";
import { FlagButtonProps } from "./FlagButton.types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./FlagButton.module.scss";

export const FlagButton: React.FC<FlagButtonProps> = ({
	icon,
	onClick,
	direction,
	className,
}) => {
	return (
		<button
			onClick={onClick}
			className={`${styles[direction]} ${styles.flag} ${className}`}
		>
			<FontAwesomeIcon icon={icon} />
		</button>
	);
};
