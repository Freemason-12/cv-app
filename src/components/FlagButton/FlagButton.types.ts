import { IconProp } from "@fortawesome/fontawesome-svg-core";
export interface FlagButtonProps {
	icon: IconProp;
	onClick: (e?: any) => void;
	direction: "up" | "down" | "left" | "right";
	className?: string;
}
