import React from "react";
import { PhotoBoxProps } from "./PhotoBox.types";
import styles from "./PhotoBox.module.scss";

export const PhotoBox: React.FC<PhotoBoxProps> = (props) => {
	const details = (
		<div className={styles.description}>
			<h2>{props.title}</h2>
			<p>{props.description}</p>
		</div>
	);
	return (
		<div className={`${styles.photobox} ${styles[props.type]}`}>
			<div>
				<img className={styles.avatar} src={props.avatar} alt="avatar" />
			</div>
			<h1 className={styles.nickname}>{props.name}</h1>
			{props.type === "long" ? details : <></>}
		</div>
	);
};
