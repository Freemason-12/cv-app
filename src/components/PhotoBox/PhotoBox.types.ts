export interface PhotoBoxProps {
  name: string;
  title: string;
  description: string;
  avatar: string;
  type: "long" | "short";
}
