import { render, screen } from "@testing-library/react";
import { PhotoBox } from "./PhotoBox";

describe("PhotoBox component", () => {
  it("should show a short version when given a 'short' prop", () => {
    render(
      <PhotoBox
        name="Test Name"
        title="Test Title"
        description="test desctiption"
        avatar="./avatar.jpg"
        type="short"
      />,
    );
    expect(screen.queryByText("Test Title")).toBeNull();
    expect(screen.queryByText("test description")).toBeNull();
  });
  it("should show a long version when given a 'long' prop", () => {
    render(
      <PhotoBox
        name="Test Name"
        title="Test Title"
        description="test description"
        avatar="./avatar.jpg"
        type="long"
      />,
    );
    expect(screen.getByText(/Test Title/)).toBeInTheDocument();
    expect(screen.getByText(/test description/)).toBeInTheDocument();
  });
});
