import { fireEvent, render, screen } from "@testing-library/react";
import { Button } from "./Button";

describe("Button component", () => {
  it("should render a button with a text", () => {
    const buttonClicked = jest.fn();
    render(<Button text="test button" onClick={buttonClicked} />);
    const button = screen.getByText(/test button/);
    expect(button).toBeInTheDocument();

    fireEvent.click(button);
    expect(buttonClicked).toBeCalled();
  });
});
