import React from "react";
import { ButtonProps } from "./Button.types";
import styles from "./Button.module.scss";

export const Button: React.FC<ButtonProps> = ({
	icon,
	text,
	onClick,
	type,
}) => {
	return (
		<button type={type} onClick={onClick} className={styles.button}>
			{icon ? <div>{icon}</div> : ""}
			{text ? <div className={styles.text}>{text}</div> : ""}
		</button>
	);
};
