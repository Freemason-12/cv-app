// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ReactElement } from "react";
export interface ButtonProps {
	icon?: ReactElement;
	text: string;
	onClick?: (e?: any) => void;
	type?: "submit" | "reset" | "button" | undefined;
}
