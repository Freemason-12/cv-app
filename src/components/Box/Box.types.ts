import { ReactElement } from "react";
export interface BoxProps {
	title: string;
	content: string | ReactElement | ReactElement[];
	innerRef?: any;
}
