import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Box } from "./Box";

describe("Box component", () => {
  it("renders a title and a content", () => {
    render(<Box title="Box title" content="box content" />);
    expect(screen.getByText(/Box title/)).toBeInTheDocument();
    expect(screen.getByText(/box content/)).toBeInTheDocument();
  });
});
