import React from "react";
import { BoxProps } from "./Box.types";
import styles from "./Box.module.scss";

export const Box: React.FC<BoxProps> = (props) => {
	return (
		<div tabIndex={-1} ref={props.innerRef} className={styles.box}>
			<h1>{props.title}</h1>
			<div className={styles.content}>{props.content}</div>
		</div>
	);
};
