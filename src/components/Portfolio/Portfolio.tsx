import React from "react";
import styles from "./Portfolio.module.scss";
import portfolioCard1 from "../../assets/images/portfolio_card_1.png";
import portfolioCard3 from "../../assets/images/portfolio_card_3.png";

export function Portfolio() {
  return (
    <>
      <label htmlFor="all">All</label>/<label htmlFor="ui">Ui</label>/
      <label htmlFor="code">Code</label>
      <ul className={styles.container}>
        <input
          type="radio"
          name="portfolio"
          id="all"
          className={styles.radioAll}
          defaultChecked
        />
        <input
          type="radio"
          name="portfolio"
          id="ui"
          className={styles.radioUi}
        />
        <input
          type="radio"
          name="portfolio"
          id="code"
          className={styles.radioCode}
        />
        <li className={styles.portfolio + " " + styles.portfolioCard1}>
          <img src={portfolioCard1} alt="portfolio_card_1" />
          <div className={styles.portfolioInfo}>
            <h2>Some text</h2>
            <p>
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
              arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
              justo. Nullam dictum felis eu pede mollis{" "}
            </p>
            <a href="https://somesite.com">View Source</a>
          </div>
        </li>
        <li className={styles.portfolio + " " + styles.portfolioCard3}>
          <img src={portfolioCard3} alt="portfolio_card_3" />
          <div className={styles.portfolioInfo}>
            <h2>Some text</h2>
            <p>
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
              arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
              justo. Nullam dictum felis eu pede mollis{" "}
            </p>
            <a href="https://somesite.com">View Source</a>
          </div>
        </li>
        <li className={styles.portfolio + " " + styles.portfolioCard1}>
          <img src={portfolioCard1} alt="portfolio_card_1" />
          <div className={styles.portfolioInfo}>
            <h2>Some text</h2>
            <p>
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
              arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
              justo. Nullam dictum felis eu pede mollis{" "}
            </p>
            <a href="https://somesite.com">View Source</a>
          </div>
        </li>
        <li className={styles.portfolio + " " + styles.portfolioCard3}>
          <img src={portfolioCard3} alt="portfolio_card_3" />
          <div className={styles.portfolioInfo}>
            <h2>Some text</h2>
            <p>
              Donec pede justo, fringilla vel, aliquet nec, vulputate eget,
              arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
              justo. Nullam dictum felis eu pede mollis{" "}
            </p>
            <a href="https://somesite.com">View Source</a>
          </div>
        </li>
      </ul>
    </>
  );
}
