import { IconProp } from "@fortawesome/fontawesome-svg-core";
export interface AddressProps {
  data: {
    icon: IconProp;
    title?: string;
    text: string;
    address: string;
  }[];
}
