import React from "react";
import { AddressProps } from "./Address.types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./Address.module.scss";

export const Address: React.FC<AddressProps> = ({ data }) => {
  return (
    <div>
      {data.map((d, i) => (
        <div key={i} className={styles.addressItem}>
          <FontAwesomeIcon icon={d.icon} />
          <div>
            {d.title ? (
              <>
                <div className={styles.title}>
                  <b>{d.title}</b>
                </div>
                <a className={styles.link} href={d.address}>
                  {d.text}
                </a>
              </>
            ) : (
              <div className={styles.title}>
                <b>
                  <a href={d.address}>{d.text}</a>
                </b>
              </div>
            )}
          </div>
        </div>
      ))}
    </div>
  );
};
