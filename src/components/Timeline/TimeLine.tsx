import React from "react";
import { TimeLineProps } from "./TimeLine.types";
import { LoadingScreen } from "../LoadingScreen/LoadingScreen";
import { ErrorScreen } from "../ErrorScreen/ErrorScreen";
import styles from "./TimeLine.module.scss";

export const TimeLine: React.FC<TimeLineProps> = ({ data, state }) => {
	return state === "pending" ? (
		<LoadingScreen />
	) : state === "error" ? (
		<ErrorScreen />
	) : (
		<table className={styles.timeline}>
			{data.map((d, i) => (
				<tbody key={i}>
					<tr>
						<td className={styles.date}>{d.date}</td>
						<td rowSpan={2}>
							<div className={styles.info}>
								<label>
									<b>{d.title}</b>
								</label>
								<div>{d.text}</div>
							</div>
						</td>
					</tr>
					<tr>
						<td className={styles.stripe}>
							<div></div>
						</td>
					</tr>
				</tbody>
			))}
		</table>
	);
};
