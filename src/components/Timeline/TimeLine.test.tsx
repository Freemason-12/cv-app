// import { toBeInTheDocument } from "@testing-library/jest-dom/matchers";
import { render, screen } from "@testing-library/react";
import { TimeLine } from "./TimeLine";

describe("TimeLine component", () => {
  const data = [
    { date: 2001, title: "Title 1", text: "text 1" },
    { date: 2002, title: "Title 2", text: "text 2" },
    { date: 2003, title: "Title 3", text: "text 3" },
    { date: 2004, title: "Title 4", text: "text 4" },
  ];
  it("should show all data given to it by props", () => {
    render(<TimeLine state="fullfilled" data={data} />);
    for (let i of data) {
      expect(screen.getByText(`${i.date}`)).toBeInTheDocument();
      expect(screen.getByText(i.title)).toBeInTheDocument();
      expect(screen.getByText(i.text)).toBeInTheDocument();
    }
  });
  it("should show loading screen when the 'state' prop is set to 'pending'", () => {
    render(<TimeLine state="pending" data={data} />);
    expect(screen.getByRole("img")).toBeInTheDocument();
  });
  it("should show error screen when the 'state' prop is set to 'error'", () => {
    render(<TimeLine state="error" data={data} />);
    expect(
      screen.getByText(
        "Something went wrong: please review your server connection",
      ),
    ).toBeInTheDocument();
  });
});
