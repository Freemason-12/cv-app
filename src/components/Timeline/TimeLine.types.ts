import { Timestamp } from "../../store/timeline/types";
import { FieldState } from "../../store/store";
export interface TimeLineProps {
	data: Timestamp[];
	state: FieldState;
}
