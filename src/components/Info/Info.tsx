import React from "react";

export const Info: React.FC<{ text: string }> = ({ text }) => <div>{text}</div>;
