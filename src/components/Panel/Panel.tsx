import React, { ReactElement } from "react";
import { PhotoBox } from "../PhotoBox/PhotoBox";
import { Navigation } from "../Navigation/Navigation";
import { Button } from "../Button/Button";
import { FlagButton } from "../FlagButton/FlagButton";
import { useNavigate } from "react-router-dom";
import { faNavicon, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "./Panel.module.scss";

type PanelProps = {
	state?: string;
	setState?: (state: string) => void;
	children?: ReactElement<typeof Navigation>;
};
export const Panel: React.FC<PanelProps> = ({ state, setState, children }) => {
	const navigate = useNavigate();
	return (
		<aside className={styles.panel + " " + (state ? styles[state] : "")}>
			<FlagButton
				icon={faNavicon}
				direction="left"
				onClick={
					setState
						? () => setState(state === "open" ? "closed" : "open")
						: () => { }
				}
				className={styles.panelFlag}
			/>
			<div>
				<PhotoBox
					type="short"
					name="John Doe"
					title="Programmer. Creative. Innovator"
					description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
					avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
				/>
				{children}
			</div>
			<Button
				onClick={() => navigate("/")}
				icon={<FontAwesomeIcon icon={faChevronLeft} />}
				text="Go Back"
			/>
		</aside>
	);
};
