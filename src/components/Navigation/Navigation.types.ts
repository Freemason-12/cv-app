import { NavItem } from "./components/NavItem/NavItem";
import { ReactElement } from "react";
export interface NavigationProps {
  children?: ReactElement<typeof NavItem> | ReactElement<typeof NavItem>[];
}
