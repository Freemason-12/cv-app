import React, { MutableRefObject } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { useVisible } from "../../../../services/useVisible";
import styles from "./NavItem.module.scss";

type NavItemProps = {
	iconType: IconProp;
	text: string;
	reference: MutableRefObject<HTMLElement | null>;
};
export const NavItem: React.FC<NavItemProps> = ({
	iconType,
	text,
	reference,
}) => {
	const { isVisible, setFocus } = useVisible(reference);
	return (
		<div
			onClick={setFocus}
			className={`${styles.navitem} ${isVisible ? styles.focused : styles.regular}`}
		>
			<FontAwesomeIcon className={styles.navicon} icon={iconType} />
			<div>{text}</div>
		</div>
	);
};
