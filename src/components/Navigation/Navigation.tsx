import React from "react";
import { NavigationProps } from "./Navigation.types";

export const Navigation: React.FC<NavigationProps> = ({ children }) => {
  return <div>{children}</div>;
};
