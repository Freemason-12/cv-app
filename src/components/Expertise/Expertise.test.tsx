import { render, screen } from "@testing-library/react";
import { Expertise } from "./Expertise";

describe("Expertise component", () => {
  it("should render all entries properly", () => {
    render(
      <Expertise
        data={[
          {
            date: "2013-2014",
            info: {
              company: "company 1",
              job: "job 1",
              description: "description 1",
            },
          },
          {
            date: "2014-2015",
            info: {
              company: "company 2",
              job: "job 2",
              description: "description 2",
            },
          },
        ]}
      />,
    );
    expect(screen.getByText("2013-2014")).toBeInTheDocument();
    expect(screen.getByText("company 1")).toBeInTheDocument();
    expect(screen.getByText("job 1")).toBeInTheDocument();
    expect(screen.getByText("description 1")).toBeInTheDocument();

    expect(screen.getByText("2014-2015")).toBeInTheDocument();
    expect(screen.getByText("company 2")).toBeInTheDocument();
    expect(screen.getByText("job 2")).toBeInTheDocument();
    expect(screen.getByText("description 2")).toBeInTheDocument();
  });
});
