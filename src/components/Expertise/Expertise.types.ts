export interface ExpertiseProps {
  data: {
    date: string;
    info: {
      company: string;
      job: string;
      description: string;
    };
  }[];
}
