import React from "react";
import { ExpertiseProps } from "./Expertise.types";
import styles from "./Expertise.module.scss";
export const Expertise: React.FC<ExpertiseProps> = ({ data }) => {
  return (
    <table>
      <tbody>
        {data.map((d, i) => (
          <tr key={i} className={styles.expertise}>
            <td className={styles.dateplace}>
              <div>
                <b>{d.info.company}</b>
              </div>
              <div>{d.date}</div>
            </td>
            <td className={styles.job}>
              <div>
                <b>{d.info.job}</b>
              </div>
              <div>{d.info.description}</div>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
