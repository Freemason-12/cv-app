import React from "react";
import styles from "./ErrorScreen.module.scss";
export const ErrorScreen: React.FC = () => (
	<div className={styles.error}>
		<p>Something went wrong: please review your server connection</p>
	</div>
);
