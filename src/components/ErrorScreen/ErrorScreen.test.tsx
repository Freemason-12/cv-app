import { toBeInTheDocument } from "@testing-library/jest-dom/matchers";
import { render, screen } from "@testing-library/react";
import { ErrorScreen } from "./ErrorScreen";

describe("ErrorScreen compoenent", () => {
  it("should show error message", () => {
    render(<ErrorScreen />);
    expect(
      screen.getByText(
        /Something went wrong: please review your server connection/,
      ),
    ).toBeInTheDocument();
  });
});
