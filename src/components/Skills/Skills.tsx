import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Skill } from "../../store/skills/types";
import { AppState } from "../../store/store";
import { useAppDispatch } from "../../store/store";
import { addSkill, setSkillsList } from "../../store/skills/thunk";
import { setPendingAction } from "../../store/skills/actions";
import { LoadingScreen } from "../LoadingScreen/LoadingScreen";
import { ErrorScreen } from "../ErrorScreen/ErrorScreen";
import styles from "./Skills.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { Button } from "../Button/Button";
import { Formik, Form, Field, ErrorMessage } from "formik";

export const Skills: React.FC = () => {
  const skills = useSelector((store: AppState) => store.skills);
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setSkillsList());
  }, []);

  const [name, setName] = useState("");
  const [level, setLevel] = useState("");

  return skills.state === "pending" ? (
    <LoadingScreen />
  ) : skills.state === "error" ? (
    <ErrorScreen />
  ) : (
    <div className={styles.skillsElement}>
      <input className={styles.formOpen} type="checkbox" id="formOpen" />
      <label className={styles.formToggle} htmlFor="formOpen">
        <FontAwesomeIcon icon={faPencilAlt} /> Open Edit
      </label>
      <Formik
        className={styles.formik}
        initialValues={{ name: "", level: "" }}
        onSubmit={() => {
          dispatch(setPendingAction());
          dispatch(addSkill({ name, level: Number(level) }));
        }}
      >
        <Form className={styles.form}>
          <div className={styles.inputField}>
            <label htmlFor="name">Skill name: </label>
            <Field
              id="name"
              name="name"
              type="name"
              value={name}
              onChange={(e: any) => setName(e.target.value)}
              validate={() => (name.length === 0 ? "name required" : undefined)}
              placeholder="Enter skill name"
            />
            <ErrorMessage
              className={styles.error}
              name="name"
              component="div"
            />
          </div>
          <div className={styles.inputField}>
            <label htmlFor="level">Skill range: </label>
            <Field
              id="level"
              name="level"
              type="level"
              validate={() =>
                Number(level) < 10
                  ? "level must be at least 10"
                  : Number(level) >= 100
                    ? "level must be at most 100"
                    : undefined
              }
              value={level}
              onChange={(e: any) =>
                setLevel(e.target.value.replaceAll(/[^0-9]/g, ""))
              }
              placeholder="Enter skill range"
            />
            <ErrorMessage
              className={styles.error}
              name="level"
              component="div"
            />
          </div>
          <Button type="submit" text="Add skill" />
        </Form>
      </Formik>

      <div className={`${styles.skills} ${styles[skills.state]}`}>
        {skills.state === "fullfilled"
          ? skills.all.map((skill: Skill, i) => (
              <div
                key={i}
                className={styles.scale}
                style={{ width: `${skill.level}%` }}
              >
                {skill.name}
              </div>
            ))
          : ""}
      </div>
      <div className={styles.ruler}>
        <div className={styles.rulerMarks}>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div className={styles.rulerNotes}>
          <div>Beginner</div>
          <div>Proficient</div>
          <div>Expert</div>
          <div>Master</div>
        </div>
      </div>
    </div>
  );
};
