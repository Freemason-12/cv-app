export interface FeedbackProps {
  data: {
    feedback: string;
    reporter: {
      photoUrl: string;
      name: string;
      citeUrl: string;
    };
  }[];
}
