import React from "react";
import { FeedbackProps } from "./Feedback.types";
import styles from "./Feedback.module.scss";

export const Feedback: React.FC<FeedbackProps> = ({ data }) => {
  return (
    <>
      {data.map((d, i) => (
        <div key={i}>
          <div className={styles.feedback}>{d.feedback}</div>
          <div className={styles.reporter}>
            <img
              className={styles.avatar}
              src={d.reporter.photoUrl}
              alt="avatar"
            />
            <div>
              {d.reporter.name},
              <a href={d.reporter.citeUrl}>{d.reporter.citeUrl}</a>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};
