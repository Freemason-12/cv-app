import { render, screen } from "@testing-library/react";
import { Feedback } from "./Feedback";

describe("Feedback component", () => {
  it("should render the text content, author and its image", () => {
    const data = [
      {
        feedback: "feedback 1",
        reporter: {
          photoUrl: "./photo.jpg",
          name: "reporter 1",
          citeUrl: "https://www.citeexample1.com",
        },
      },
      {
        feedback: "feedback 2",
        reporter: {
          photoUrl: "http://avatars0.githubusercontent.com/u/246180?v=4",
          name: "John Doe",
          citeUrl: "https://www.citeexample2.com",
        },
      },
    ];
    render(<Feedback data={data} />);
    for (let i of data) {
      expect(screen.getByText(i.feedback)).toBeInTheDocument();
      expect(screen.getByText(`${i.reporter.name},`)).toBeInTheDocument();
      expect(screen.getByText(i.reporter.citeUrl)).toBeInTheDocument();
    }
    const avatars = screen.getAllByAltText("avatar");
    expect(avatars.length).toEqual(data.length);
    for (let i of avatars) expect(i).toBeInTheDocument();
  });
});
