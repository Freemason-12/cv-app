import React, { useState, useRef, useEffect } from "react";
import { useSelector } from "react-redux";
import { setTimeline } from "../../store/timeline/thunk";
import { AppState, useAppDispatch } from "../../store/store";
import { Box } from "../../components/Box/Box";
import { Expertise } from "../../components/Expertise/Expertise";
import { Feedback } from "../../components/Feedback/Feedback";
import { Navigation } from "../../components/Navigation/Navigation";
import { NavItem } from "../../components/Navigation/components/NavItem/NavItem";
import { Panel } from "../../components/Panel/Panel";
import { Portfolio } from "../../components/Portfolio/Portfolio";
import { TimeLine } from "../../components/Timeline/TimeLine";
import { Address } from "../../components/Address/Address";
import { FlagButton } from "../../components/FlagButton/FlagButton";
import { Skills } from "../../components/Skills/Skills";
import {
  faPhone,
  faEnvelope,
  faUser,
  faGraduationCap,
  faGem,
  faPen,
  faSuitcase,
  faLocationArrow,
  faComment,
  faChevronUp,
} from "@fortawesome/free-solid-svg-icons";
import {
  faTwitter,
  faSkype,
  faFacebook,
} from "@fortawesome/free-brands-svg-icons";
import "./Inner.scss";

export const Inner: React.FC = () => {
  const [panelState, setPanelState] = useState("closed");
  const about = useRef<HTMLElement>(null);
  const education = useRef<HTMLElement>(null);
  const experience = useRef<HTMLElement>(null);
  const skills = useRef<HTMLElement>(null);
  const portfolio = useRef<HTMLElement>(null);
  const contacts = useRef<HTMLElement>(null);
  const feedback = useRef<HTMLElement>(null);

  const timeline = useSelector((store: AppState) => store.timeline);
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(setTimeline());
  }, [dispatch]);
  return (
    <>
      <FlagButton
        className="backToTop"
        onClick={() => about.current?.focus()}
        icon={faChevronUp}
        direction="up"
      />
      <Panel state={panelState} setState={setPanelState}>
        <Navigation>
          <NavItem reference={about} iconType={faUser} text="About me" />
          <NavItem
            reference={education}
            iconType={faGraduationCap}
            text="Education"
          />
          <NavItem reference={experience} iconType={faPen} text="Experience" />
          <NavItem reference={skills} iconType={faGem} text="Skills" />
          <NavItem
            reference={portfolio}
            iconType={faSuitcase}
            text="PortFolio"
          />
          <NavItem
            reference={contacts}
            iconType={faLocationArrow}
            text="Contacts"
          />
          <NavItem reference={feedback} iconType={faComment} text="Feedback" />
        </Navigation>
      </Panel>
      <div className={`main ${panelState}`}>
        <Box
          innerRef={about}
          title="About me"
          content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque"
        />
        <Box
          innerRef={education}
          title="Education"
          content={<TimeLine state={timeline.state} data={timeline.all} />}
        />
        <Box
          innerRef={experience}
          title="Experience"
          content={
            <Expertise
              data={[
                {
                  date: "2013-2014",
                  info: {
                    company: "Google",
                    job: "Front-end developer / php programmer",
                    description:
                      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
                  },
                },
                {
                  date: "2012",
                  info: {
                    company: "Twitter",
                    job: "Web developer",
                    description:
                      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
                  },
                },
              ]}
            />
          }
        />
        <Box innerRef={skills} title="Skills" content={<Skills />} />
        <Box innerRef={portfolio} title="Portfolio" content={<Portfolio />} />
        <Box
          innerRef={contacts}
          title="Contacts"
          content={
            <Address
              data={[
                {
                  icon: faPhone,
                  text: "500 342 342",
                  address: "tel:500342342",
                },
                {
                  icon: faEnvelope,
                  text: "office@kamsolutions.pl",
                  address: "mailto:office@kamsolutions.pl",
                },
                {
                  icon: faTwitter,
                  title: "Twitter",
                  text: "https://twitter.com/wordpress",
                  address: "https://twitter.com/wordpress",
                },
                {
                  icon: faFacebook,
                  title: "Facebook",
                  text: "https://facebook.com/facebook",
                  address: "https://facebook.com/facebook",
                },
                {
                  icon: faSkype,
                  title: "Skype",
                  text: "kamsolutions.pl",
                  address: "kamsolutions.pl",
                },
              ]}
            />
          }
        />
        <Box
          innerRef={feedback}
          title="Feedback"
          content={
            <Feedback
              data={[
                {
                  feedback:
                    " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
                  reporter: {
                    photoUrl:
                      "http://avatars0.githubusercontent.com/u/246180?v=4",
                    name: "John Doe",
                    citeUrl: "https://www.citeexample.com",
                  },
                },
                {
                  feedback:
                    " Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor",
                  reporter: {
                    photoUrl:
                      "http://avatars0.githubusercontent.com/u/246180?v=4",
                    name: "John Doe",
                    citeUrl: "https://www.citeexample.com",
                  },
                },
              ]}
            />
          }
        />
      </div>
    </>
  );
};
