import React from "react";
import { Link } from "react-router-dom";
import { Button } from "../../components/Button/Button";
import { PhotoBox } from "../../components/PhotoBox/PhotoBox";
import "./Home.scss";
export const Home: React.FC = () => (
  <div className="home">
    <div>
      <PhotoBox
        type="long"
        name="John Doe"
        title="Programmer. Creative. Innovator"
        description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
        avatar="http://avatars0.githubusercontent.com/u/246180?v=4"
      />
      <Link to="/inner">
        <Button text="Know more" />
      </Link>
    </div>
  </div>
);
